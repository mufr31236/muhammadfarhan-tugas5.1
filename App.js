import React from 'react';
import {Provider} from 'react-redux';
import {persistor, store} from './src/stateManagement/utils/store';
import {PersistGate} from 'redux-persist/integration/react';
import {SafeAreaView, StatusBar} from 'react-native';
import Routing from './src/stateManagement/Routing';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaView style={{flex: 1}}>
          <StatusBar barStyle={'dark-content'} />
          <Routing />
        </SafeAreaView>
      </PersistGate>
    </Provider>
  );
};

export default App;
