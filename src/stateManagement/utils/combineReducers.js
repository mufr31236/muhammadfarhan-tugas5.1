import {combineReducers} from 'redux';
import authReducer from './reducers/authReducers';

const rootReducers = combineReducers({
  auth: authReducer,
});

export default rootReducers;
