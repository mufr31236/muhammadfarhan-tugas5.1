import AsyncStorage from '@react-native-async-storage/async-storage';
import rootReducers from './combineReducers';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  timeout: null,
};

const persistReducers = persistReducer(persistConfig, rootReducers);

const store = createStore(persistReducers, applyMiddleware(thunk));

let persistor = persistStore(store);

export {store, persistor};
