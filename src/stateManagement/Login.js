import {
  View,
  Text,
  ToastAndroid,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';

const Login = ({navigation, route}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const {isLoggedIn, userData} = useSelector(state => state.auth);
  const url = 'https://staging.api.autotrust.id/api/v1/';

  const dispatch = useDispatch();

  const onLogin = async () => {
    const data = {
      email,
      password,
    };
    try {
      const response = await fetch(`${url}user/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
      const result = await response.json();
      console.log('Success: ', result);
      console.log('Success: ', response);
      if (result.code === 200) {
        const data = result;
        dispatch({type: 'LOGIN_SUCCESS', data});
        ToastAndroid.showWithGravity(
          'Login Sukses',
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
        );
        navigation.replace('home');
      }
    } catch (e) {
      console.log('ERROR: ', e);
      ToastAndroid.showWithGravity(
        'Login gagal, email atau password salah',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
      );
    }
  };

  React.useEffect(() => {
    console.log('data user : ', isLoggedIn, 'data : ', userData);
    if (isLoggedIn) {
      navigation.replace('home');
    }
  }, []);

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{fontSize: 20, color: '#000', fontWeight: '700'}}>
        Login
      </Text>
      <View style={{width: '80%', marginTop: 15}}>
        <Text>Masukan Email</Text>
        <TextInput
          value={email}
          onChangeText={text => setEmail(text)}
          placeholder="Masukkan Email"
          keyboardType="email-address"
          style={{
            width: '100%',
            borderRadius: 6,
            borderColor: '#dedede',
            borderWidth: 1,
            paddingHorizontal: 10,
            marginTop: 15,
          }}
        />
      </View>
      <View style={{width: '80%', marginTop: 15}}>
        <Text>Masukkan Password</Text>
        <TextInput
          value={password}
          onChangeText={pass => setPassword(pass)}
          placeholder="Masukkan Password"
          secureTextEntry={true}
          style={{
            width: '100%',
            borderRadius: 6,
            borderColor: '#dedede',
            borderWidth: 1,
            paddingHorizontal: 10,
            marginTop: 15,
          }}
        />
      </View>
      <TouchableOpacity
        onPress={() => onLogin()}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: 10,
          paddingHorizontal: 12,
          backgroundColor: 'green',
          borderRadius: 6,
          marginTop: 20,
        }}>
        <Text style={{color: '#fff', fontSize: 14, fontWeight: '500'}}>
          Log in
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Login;
